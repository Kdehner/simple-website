// navbar logic

const toggle = document.querySelector('.toggle');
const navItems = document.querySelectorAll('.nav-item');

toggle.addEventListener('click', toggleMenu);

function toggleMenu() {
  navItems.forEach(item => {
    if (item.classList.contains('active')) {
      item.classList.remove('active');
    } else {
      item.classList.add('active');
    }
  });
  if (this.childNodes[0].innerHTML === '<i class="fas fa-bars" aria-hidden="true"></i>') {
    this.childNodes[0].innerHTML = '<i class="fas fa-times" aria-hidden="true"></i>'
  } else {
    this.childNodes[0].innerHTML = '<i class="fas fa-bars" aria-hidden="true"></i>'
  }
}